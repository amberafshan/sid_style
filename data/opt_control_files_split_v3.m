function opt_control_files_split_v3(enroll, test,data_dir, fd, fe, ft)
load('seed');
if(fe==1)
    if exist('spk_train', 'dir')
        rmdir 'spk_train' s;
    end
    
    mkdir 'spk_train';
end

if(ft==1)
    if exist('spk_test', 'dir')
        rmdir 'spk_test' s;
    end
    
    mkdir 'spk_test';
end

if(fd==1)
    if exist('dev', 'dir')
        rmdir 'dev' s;
    end
    mkdir 'dev';
    
    if exist('dev_orig', 'dir')
        rmdir 'dev_orig' s;
    end
    mkdir 'dev_orig';
    
end






%read vs others
enroll_dir = [data_dir enroll '/']; %%% If its easier just edit this directly to input enroll dir
test_dir = [data_dir test '/']; %%% Test dir

t_nt_cell = {'nontarget','target'};
GENDERS = {'FEMALE','MALE'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for nG = 1:length(GENDERS)
    currGender = GENDERS{nG};
    list = textread([currGender '_list'],'%s');
    rng(s);
    l=length(list);
    ids_shuff = randperm(l);
    enroll_ids = list(ids_shuff(1:49));
    test_ids = list(ids_shuff(50:end));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if(fd==1)
        for cnt_in = enroll_ids'
            system(['ls ' enroll_dir currGender '_SET*/' char(cnt_in) '*.wav >> dev/wav']);
            system(['ls ' enroll_dir currGender '_SET*/' char(cnt_in) '*.wav >> dev_orig/wav']);
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for cnt_in = test_ids'
        if(contains(enroll,'Sentences')) %%%for read trying to get only session A for eval
            system(['ls ' enroll_dir currGender '_SET*/' char(cnt_in) '*A_*.wav >> spk_train/wav']);
        else
            if(contains(enroll,'Phone'))
                system(['ls ' enroll_dir currGender '_SET*/' char(cnt_in) '*01.wav >> spk_train/wav']);
                
            else
                system(['ls ' enroll_dir currGender '_SET*/' char(cnt_in) '*.wav >> spk_train/wav']);
            end
            
        end
        if(contains(test,'Sentences'))  %%%for read trying to get only session C for test
            system(['ls ' test_dir currGender '_SET*/' char(cnt_in) '*C_*.wav >> spk_test/wav']);
            
        else
            if(contains(test,'Phone'))
                system(['ls ' test_dir currGender '_SET*/' char(cnt_in) '*02.wav >> spk_test/wav']);
            else
                system(['ls ' test_dir currGender '_SET*/' char(cnt_in) '*.wav >> spk_test/wav']);
            end
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
if(fd==1)
    system("cat dev/wav | rev | cut -f 1 -d '/' | rev | cut -f 1 -d '.' > dev/utt");
    system("cat dev/wav | rev | cut -f 1 -d '/' | rev | cut -c 1-3 > dev/spk");
    
    system("paste dev/utt dev/wav > dev/wav.scp");
    system("paste dev/utt dev/spk > dev/utt2spk");
    
    system("cat dev_orig/wav | rev | cut -f 1 -d '/' | rev | cut -f 1 -d '.' > dev_orig/utt");
    system("cat dev_orig/wav | rev | cut -f 1 -d '/' | rev | cut -c 1-3 > dev_orig/spk");
    
    system("sed -i 's/$/_orig/' dev_orig/utt");
    system("paste dev_orig/utt dev_orig/wav > dev_orig/wav.scp");
    system("paste dev_orig/utt dev_orig/spk > dev_orig/utt2spk");
end
system("cat spk_train/wav | rev | cut -f 1 -d '/' | rev | cut -f 1 -d '.' > spk_train/utt");
system("cat spk_train/wav | rev | cut -f 1 -d '/' | rev | cut -c 1-3 > spk_train/spk");

system("paste spk_train/utt spk_train/wav > spk_train/wav.scp");
system("paste spk_train/utt spk_train/spk > spk_train/utt2spk");

system("cat spk_test/wav | rev | cut -f 1 -d '/' | rev | cut -f 1 -d '.' > spk_test/utt");
system("cat spk_test/wav | rev | cut -f 1 -d '/' | rev | cut -c 1-3 > spk_test/spk");

system("paste spk_test/utt spk_test/wav > spk_test/wav.scp");
system("paste spk_test/utt spk_test/spk > spk_test/utt2spk");
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cnt=1;
fid=fopen('spk_train/spk');
while ~feof(fid)
    tline = fgets(fid);
    temp=regexp(tline,'[\r\f\n]','split');
    spkr_list{cnt} = temp{1};
    cnt=cnt+1;
end
fclose(fid);

cnt=1;
fid=fopen('spk_test/utt');
while ~feof(fid)
    tline = fgets(fid);
    temp=regexp(tline,'[\r\f\n]','split');
    utt_list{cnt} = temp{1};
    cnt=cnt+1;
end
fclose(fid);

fid=fopen('spk_test/trials','w');
for cnt_spk = 1:length(spkr_list)
    for cnt_utt = 1:length(utt_list)
        t_nt = strcmp(spkr_list{cnt_spk},utt_list{cnt_utt}(1:3));
        fprintf(fid,[spkr_list{cnt_spk} ' ' utt_list{cnt_utt} ' ' t_nt_cell{t_nt+1} '\n']);
        
    end
end
fclose(fid);

end
