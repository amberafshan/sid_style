clear;clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% NEED TO EDIT THIS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath '/media/amber/tools/sap-voicebox/voicebox'
frame_shift = 10
data_dir = '/media/amber/databases/UCLA_SVD_11_20_2018/';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
wavs_list = 'dev/wav';
cnt=1;
fid=fopen(wavs_list);
while ~feof(fid)
    tline = fgets(fid);
    temp_in = regexp(tline,'[\r\f\n]','split');
    temp = strsplit(temp_in{1});
    fileNames{cnt,1} = temp{1};
    cnt=cnt+1;
end
fclose(fid);
fs=8000;
if exist('dev_vad.txt', 'file')
    delete('dev_vad.txt')
end

fid = fopen('dev_vad.txt','w');

for cnt=1:length(fileNames)
    if(mod(cnt,1000)==0)
        disp(cnt)
    end
    fileNamein = fileNames{cnt};
    fileName = erase(fileNamein,data_dir);
    
    [snd,fsOrig] = audioread([data_dir fileName]);
    snd = resample(snd,fs,fsOrig);
    [vs,~] = vadsohn(snd,fs);
    
    data = VFR_kaldi(snd,fs,frame_shift,[vs;zeros(length(snd)-length(vs),1)]');
    
    inds = strfind(fileName,'/');
    
    fprintf(fid, '%s [ ', fileName(inds(end)+1:end-4));
    
    ncol = length(data);
    
    fmt = repmat('%0.1f ', 1, ncol);
    
    fprintf(fid, fmt, data.' );
    fprintf(fid, ' ]\n');
end

fclose(fid);
