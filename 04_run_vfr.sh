#!/bin/bash


echo "Today is $(date)" >> results_all
#######################################################################
#place where the SVD data is stored
#Note that here we are using the 30sec segments
data_dir= ###
#######################################################################

#write matlab code into this file
matlab_file=matlab_code.sh

stage=0
fd=1
fe=1
ft=1
flag=true
#######################################################################

for enroll in {26ChopSentences_30sec,66ChopPhonecalls_30sec,46ChopPrompts_30sec,76ChopInstructions_30sec,56ChopVideo_30sec}; do
	fe=1
	for test in {26ChopSentences_30sec,66ChopPhonecalls_30sec,46ChopPrompts_30sec,76ChopInstructions_30sec,56ChopVideo_30sec}; do
	        for vfr in {0,1,2}; do
		rm $matlab_file
		echo '#!/bin/bash' >> $matlab_file
		echo 'cd data/' >> $matlab_file
		
		echo "matlab -nodisplay -nojvm -nodesktop -r \"opt_control_files_split_v3('$enroll','$test','$data_dir',$fd,$fe,$ft); quit\"" >> $matlab_file
		if [ $fe -eq 1 ]; then
			echo "matlab -nodisplay -nojvm -nodesktop -r \"vfr_wrapper; quit\"" >> $matlab_file
		fi
		echo 'cd ..' >> $matlab_file
		sh $matlab_file
		echo "-----------------------------------------" >> results_all
                echo 'x-vectors '$enroll $test >> results_all
		echo 'VFR ' $vfr >> results_all
		sh opt_run_test_v2.sh $stage $fd $fe $ft $vfr		
		fe=0
		done
	done
done
